/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class SimpleActor extends Actor {

  /** @override */
  getRollData() {
    const data = super.getRollData();
    const shorthand = game.settings.get("myrhias", "macroShorthand");

    // Re-map all attributes onto the base roll data
    if ( !!shorthand ) {
      for ( let [k, v] of Object.entries(data.attributes) ) {
        if ( !(k in data) ) data[k] = v.value;
      }
      delete data.attributes;
    }

    // Map all items data using their slugified names
    data.items = this.data.items.reduce((obj, i) => {
      let key = i.name.slugify({strict: true});
      let itemData = duplicate(i.data);
      if ( !!shorthand ) {
        for ( let [k, v] of Object.entries(itemData.attributes) ) {
          if ( !(k in itemData) ) itemData[k] = v.value;
        }
        delete itemData["attributes"];
      }
      obj[key] = itemData;
      return obj;
    }, {});
    return data;
  }

  // fill skills value on sheet load if there is already one jobs selected for current actor
	prepareData(s=undefined) {
    super.prepareData();
		const  actorData = this.data;
    const  data = actorData.data;
    let z = data.class.value  
    let a = data.job.value
    let b = data.race.value
    let c = data.stuff.arme
    let d = data.stuff.armure
    let h = data.job.selected_skills

		// verifying var "a" contain job name and executing only if its a character
		// same thing for "b" and races
		if (actorData.type === "character" && z != "" && z != undefined && a != "" && a != undefined || b != "" && b != undefined ||  h != "" && h != undefined) {
			Object.keys(data.skills).forEach(function (feature){
				Object.keys(data.skills[feature]).forEach(function (name){
					// attribution des bonus de stat liés au métié choisi
					if (data.jobs[z] != undefined) {
						if (data.jobs[z][a] != undefined && data.jobs[z][a][name] != undefined) {
							data.skills[feature][name].base = Number(data.jobs[z][a][name])
						}
					}
					if (data.races[b] != undefined && data.races[b][name] != undefined) {
            data.skills[feature][name].base += Number(data.races[b][name])
					} 
					if ( data.skills[feature] == "special"  ) {
						data.specialskill.label = data.skills[feature][name].label
					}
				});        
			});
			// traitement special pour les dons, on boucle sur la liste de tout les dons et on attribue ceux de la race selectionné à la fiche perso avec toutes leur propriété
			if ( data.races[b] != undefined ) {
				var dons = [{}];
				delete dons[0]; 
				Object.keys(data.races[b].don).forEach(function (name){
					data.skills.don[name].base += Number(data.races[b].don[name])
					dons.push(data.skills.don[name]);
				});
        Object.assign(data.race.don, dons);
        
        // majoration/minorations caracteristiques en fonction de la rce sélectionnées
				data.race.tai.min = data.races[b].tai.min;
				data.race.tai.max = data.races[b].tai.max;

				data.race.agi.min = data.races[b].agi.min;
				data.race.agi.max = data.races[b].agi.max;

				data.race.app.min = data.races[b].app.min;
        data.race.app.max = data.races[b].app.max;
			}
		}

    // skill bonuses
		if (actorData.type === "character" && h != "" && h != undefined && Object.keys(h).length !== 0 ) {
			var skills = [{}];
			delete skills[0]; 
			var newSkillObject = {};
			Object.assign(newSkillObject, h.split(','));
			Object.keys(newSkillObject).forEach(function (name){
				skills.push(name);
			});
			data.job.bonus_skills = newSkillObject;

		}

    //weapons
    if (actorData.type === "character" && c != "" && c != undefined ) {
      Object.keys(c).forEach(function (s){
          if (c[s].value != "" && c[s].value != undefined) {
            let arme = c[s].value;
            data.stuff.arme[s].range = data.armes[arme].range;
            data.stuff.arme[s].damage = data.armes[arme].damage;
            data.stuff.arme[s].malus = data.armes[arme].malus;
          }
			});
    }
    
    //armor
		if (actorData.type === "character" && d != "" && d != undefined ) {
      Object.keys(d).forEach(function (s){
        if (d[s].value != "" && d[s].value != undefined) {
          let armure = d[s].value;
          data.stuff.armure[s].malus = data.armures[armure].malus;
          data.stuff.armure[s].bonus = data.armures[armure].bonus;
          data.stuff.armure[s].deg_bloque = data.armures[armure].deg_bloque;
        }
      });
		}	
		// setup pv_max et pe_max
		// head, arms and legs
		data.features.max_hp.head = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.leftarm = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.rightarm = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.leftleg = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
    data.features.max_hp.rightleg = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);

		// torso	
		data.features.max_hp.torso = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 2);
		// feet
		data.features.max_hp.feet = 4
		
		// pe for food sleep and power
		data.features.max_pe.food = Math.floor((Number(data.features.taille.value) + Number(data.features.pouvoir.value))/ 2 + 6);
		data.features.max_pe.sleep = Math.floor((Number(data.features.pouvoir.value) + Number(data.features.constitution.value))/ 2 + 6);
		data.features.max_pe.power = data.features.pouvoir.value;
   
		// calcul moyenne reput
		data.reput.mondiale.range1 = Math.floor((data.reput.royaume_augustin.range1 + data.reput.republique_naine.range1 + data.reput.terres_libres.range1 + data.reput.terre_du_khran.range1 + data.reput.cena.range1 + data.reput.empire_khalim.range1 + data.reput.nation_pirate.range1) / 7);
		data.reput.mondiale.range2 = Math.floor((data.reput.royaume_augustin.range2 + data.reput.republique_naine.range2 + data.reput.terres_libres.range2 + data.reput.terre_du_khran.range2 + data.reput.cena.range2 + data.reput.empire_khalim.range2 + data.reput.nation_pirate.range2) / 7);

    //setup initiative
    data.features.initiative.value = Math.round((Number(data.features.dextérité.value) + Number(data.features.agilité.value) + Number(data.features.sens.value)) / 3)
    
		//setup malus/bonus height
		switch (true) {
			case (Number(data.features.taille.value) >= 4 && Number(data.features.taille.value) <= 6):
				data.features.bonus_taille.value = 10;
				data.features.malus_taille.value = -10;
        break;
			case (Number(data.features.taille.value) >= 7 && Number(data.features.taille.value) <= 9):
				data.features.bonus_taille.value = 5;
				data.features.malus_taille.value = -5;
        break;
			case (Number(data.features.taille.value) >= 10 && Number(data.features.taille.value) <= 12):
				data.features.bonus_taille.value = 0;
				data.features.malus_taille.value = 0;
        break;
			case (Number(data.features.taille.value) >= 13 && Number(data.features.taille.value) <= 15):
				data.features.bonus_taille.value = 5;
				data.features.malus_taille.value = -5;
        break;
			case (Number(data.features.taille.value) >= 16 && Number(data.features.taille.value) <= 18):
				data.features.bonus_taille.value = 10;
				data.features.malus_taille.value = -10;
        break;
      default:
        data.features.bonus_taille.value = 0;
				data.features.malus_taille.value = 0;
	  }
		  console.log(data.features.bonus_taille)

    let forceconst = Number(data.features.force.value) + Number(data.features.taille.value);
		if (forceconst >= 25 && forceconst <= 32) {
			data.features.bonus_deg.value = "1d3";
		} else if (forceconst >= 33 && forceconst <= 40) {
			data.features.bonus_deg.value = "1d6";
		} else if (forceconst >= 41 && forceconst <= 60) {
			data.features.bonus_deg.value = "2d6";
    } else { data.features.bonus_deg.value = "" }
    
    Handlebars.registerHelper("multiplydon", function(value1=0, value2=0, value3=0, value4=0) {
      value1 = parseFloat(value1);
      value2 = parseFloat(value2);
      value3 = parseFloat(value3);
      value4 = Number(data.features[value4].value);
  
      return value1 + value2 + (value3 * value4);
    });

    Handlebars.registerHelper("getFeatureLabel", function(feature) {
      return data.features[feature].label;
    });

    Handlebars.registerHelper("getFeatureColor", function(feature) {
      return data.features[feature].color;
    });
  }

}
