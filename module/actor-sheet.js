/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SimpleActorSheet extends ActorSheet {

	/** @override */
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ["myrhias", "sheet", "actor"],
			template: "systems/myrhias/templates/actor-sheet.html",
			width: 1819,
			height: 849,
			tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
		});
	}

	/* -------------------------------------------- */
	/* -------------------------------------------- */

	/** @override */
	activateListeners(html) {
		super.activateListeners(html);

		// Everything below here is only needed if the sheet is editable
		if (!this.options.editable) return;

		// Update Inventory Item
		html.find('.item-edit').click(ev => {
		const li = $(ev.currentTarget).parents(".item");
		const item = this.actor.getOwnedItem(li.data("itemId"));
		item.sheet.render(true);
		});

		// Delete Inventory Item
		html.find('.item-delete').click(ev => {
			const li = $(ev.currentTarget).parents(".item");
			this.actor.deleteOwnedItem(li.data("itemId"));
			li.slideUp(200, () => this.render(false));
		});

		//setup skills data on class select change
		html.find(".selectclass").change(ev => {
			let z = $(ev.currentTarget).val();
			this.prepareData({z})
		});

		//setup skill bonus selected 
		html.find(".selectSkill").change(ev => {
			let h = $(ev.currentTarget).val();
			this.prepareData({h})
		});
		
		//setup skills data on jobs select change
		html.find(".selectjob").change(ev => {
			let a = $(ev.currentTarget).val();
			this.prepareData({a})
		});

		//setup skills data on race select change
		html.find(".selectrace").change(ev => {
			let b = $(ev.currentTarget).val();
			this.prepareData({b})
		});

		// setup weapon or armor spec based on selected item
		html.find(".weaponslot1, .weaponslot2, .weaponslot3, .weaponslot4").change(ev => {
			let c = $(ev.currentTarget).val();
			let s = $(ev.currentTarget).attr('class').split(' ');
			s =s[0];
			s = s.substring(6, 11);		
			this.prepareData({c, s})
		});
		html.find(".armor_tete, .armor_bras_gauche, .armor_bras_droit, .armor_buste, armor_jambe_gauche, armor_jambe_droite").change(ev => {
			let d = $(ev.currentTarget).val();
			let s = $(ev.currentTarget).attr('class').split(' ');
			s =s[0];
			s = s.substring(5, 10);
			this.prepareData({d, s})
		});

		// Rollables.
		html.find('.rollable').on('click', this._onRollable.bind(this, 'DIAL'));
		html.find('.money').on('click', this._money.bind(this));

	}

	/* -------------------------------------------- */

	/**
	 * setup skills base value and override with jobs bonus
	 * @author aymeric
	 * @param job_selected
	 * @param h selected skill to add bonus
	 * @param z selected class
	 * @param a selected job
	 * @param b selected race
	 * @param c selected weapon
	 * @param d selected armor
	 * @param s array of weapons/armors
	 */	
	prepareData({h=undefined, z=undefined, a=undefined, b=undefined, c=undefined, d=undefined, s=undefined}={}) {
		const  actorData = this.actor.data;
		const  data = actorData.data;
		
		// verifying var "a" contain job name and executing only if its a character
		// same thing for "b" and races
		if (actorData.type === "character" && z != "" && z != undefined && a != "" && a != undefined || b != "" && b != undefined) {
			Object.keys(data.skills).forEach(function (feature){
				Object.keys(data.skills[feature]).forEach(function (name){
					// attribution des bonus de stat liés au métié choisi
					if (data.jobs[z] != undefined) {
						if (data.jobs[z][a] != undefined && data.jobs[z][a][name] != undefined) {
							data.skills[feature][name].base += Number(data.jobs[z][a][name])
						}
					}
					if (data.races[b] != undefined && data.races[b][name] != undefined) {
            			data.skills[feature][name].base += Number(data.races[b][name])
					} 
					if ( data.skills[feature] == "special"  ) {
						data.specialskill.label = data.skills[feature][name].label
					}
				});        
			});
			// traitement special pour les dons, on boucle sur la liste de tout les dons et on attribue ceux de la race selectionné à la fiche perso avec toutes leur propriété
			if ( data.races[b] != undefined ) {
				var dons = [{}];
				delete dons[0]; 
				Object.keys(data.races[b].don).forEach(function (name){
					data.skills.don[name].base += Number(data.races[b].don[name])
					dons.push(data.skills.don[name]);
				});
				Object.assign(data.race.don, dons);
				
				// majoration/minorations caracteristiques en fonction de la rce sélectionnées
				data.race.tai.min = data.races[b].tai.min;
				data.race.tai.max = data.races[b].tai.max;

				data.race.agi.min = data.races[b].agi.min;
				data.race.agi.max = data.races[b].agi.max;

				data.race.app.min = data.races[b].app.min;
				data.race.app.max = data.races[b].app.max;
			}
		}

		// skill bonuses
		if (h != "" && h != undefined ) {
			var skills =  [{}];
			var newSkillObject = {};
			Object.assign(newSkillObject, h.split(','));
			Object.keys(newSkillObject).forEach(function (name){
				skills.push(name);
			});
			data.job.bonus_skills = newSkillObject;
			console.log(typeof(data.job.selected_skills) )
			console.log(data.job.selected_skills)
		}

		//weapons
		if (actorData.type === "character" && c != "" && c != undefined ) {
			Object.keys(c).forEach(function (s){
				if (c[s].value != "" && c[s].value != undefined) {
					let arme = c[s].value;
					data.stuff.arme[s].range = data.armes[arme].range;
					data.stuff.arme[s].damage = data.armes[arme].damage;
					data.stuff.arme[s].malus = data.armes[arme].malus;
				}
			});
		}

		//armor
		if (actorData.type === "character" && d != "" && d != undefined ) {
			Object.keys(d).forEach(function (s){
				if (d[s].value != "" && d[s].value != undefined) {
					let armure = d[s].value;
					data.stuff.armure[s].malus = data.armures[armure].malus;
					data.stuff.armure[s].bonus = data.armures[armure].bonus;
					data.stuff.armure[s].deg_bloque = data.armures[armure].deg_bloque;
				}
			});
		}			
		// setup pv_max et pe_max
		// head, arms and legs
		data.features.max_hp.head = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.leftaarm = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.rightarm = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.leftleg = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		data.features.max_hp.rightleg = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 4);
		// torso	
		data.features.max_hp.torso = Math.floor((Number(data.features.taille.value) + Number(data.features.constitution.value))/ 2);
		// feet
		data.features.max_hp.feet = 4
		
		// pe for food sleep and power
		data.features.max_pe.food = Math.floor((Number(data.features.taille.value) + Number(data.features.pouvoir.value))/ 2 + 6);
		data.features.max_pe.sleep = Math.floor((Number(data.features.pouvoir.value) + Number(data.features.constitution.value))/ 2 + 6);
		data.features.max_pe.power = data.features.pouvoir.value;
	
		// calcul moyenne reput
		data.reput.mondiale.range1 = Math.floor((data.reput.royaume_augustin.range1 + data.reput.republique_naine.range1 + data.reput.terres_libres.range1 + data.reput.terre_du_khran.range1 + data.reput.cena.range1 + data.reput.empire_khalim.range1 + data.reput.nation_pirate.range1) / 7);
		data.reput.mondiale.range2 = Math.floor((data.reput.royaume_augustin.range2 + data.reput.republique_naine.range2 + data.reput.terres_libres.range2 + data.reput.terre_du_khran.range2 + data.reput.cena.range2 + data.reput.empire_khalim.range2 + data.reput.nation_pirate.range2) / 7);

		//setup initiative
		data.features.initiative.value = Math.round((Number(data.features.dextérité.value) + Number(data.features.agilité.value) + Number(data.features.sens.value)) / 3)

		//setup malus/bonus height
		switch (true) {
			case (Number(data.features.taille.value) >= 4 && Number(data.features.taille.value) <= 6):
				data.features.bonus_taille.value = 10;
				data.features.malus_taille.value = -10;
        		break;
			case (Number(data.features.taille.value) >= 7 && Number(data.features.taille.value) <= 9):
				data.features.bonus_taille.value = 5;
				data.features.malus_taille.value = -5;
        		break;
			case (Number(data.features.taille.value) >= 10 && Number(data.features.taille.value) <= 12):
				data.features.bonus_taille.value = 0;
				data.features.malus_taille.value = 0;
        		break;
			case (Number(data.features.taille.value) >= 13 && Number(data.features.taille.value) <= 15):
				data.features.bonus_taille.value = 5;
				data.features.malus_taille.value = -5;
        		break;
			case (Number(data.features.taille.value) >= 16 && Number(data.features.taille.value) <= 18):
				data.features.bonus_taille.value = 10;
				data.features.malus_taille.value = -10;
        		break;
      		default:
        		data.features.bonus_taille.value = 0;
				data.features.malus_taille.value = 0;
	  	}
		  console.log(data.features.bonus_taille)

		let forceconst = Number(data.features.force.value) + Number(data.features.constitution.value);
		if (forceconst >= 25 && forceconst <= 32) {
			data.features.bonus_deg.value = "1d3"
		} else if (forceconst >= 33 && forceconst <= 40) {
			data.features.bonus_deg.value = "1d6"
		} else if (forceconst >= 41 && forceconst <= 60) {
			data.features.bonus_deg.value = "2d6"
		} else { data.features.bonus_deg.value = "" }

		// custom helper to calculate multiplier
		Handlebars.registerHelper("multiplydon", function(value1=0, value2=0, value3=0, value4=0) {
			value1 = parseFloat(value1);
			value2 = parseFloat(value2);
			value3 = parseFloat(value3);
			value4 = Number(data.features[value4].value);
		
			return value1 + value2 + (value3 * value4);
		});
	}

	/**
   * Listen for click events on rollables.
   * @param {MouseEvent} event
   */
  async _onRollable( rollmove, event) {
	// Initialize variables.
	//alert(rollmove + event);
	// dial = window , move = roll 
	let a ='';
	if (rollmove == 'MOVE') {
		a = event[4]
	} else if (rollmove == 'DIAL') {
		console.log(event)
		event.preventDefault();
		a = event.currentTarget;
	}
    let data = a.dataset;
    const actorData = this.actor.data.data;
	let formula = null;
	let pourcent = null;
    let message = null;
    let color = null;
	let templateData = {};

	if (rollmove == 'MOVE') {
		//event[0] = nbdice
		//event[1] = bonus
		//event[2] = malus
		//event[3] = multi
		//event[4] = comp
		// Handle rolls coming directly from roll dialog box with options.
		formula = event[0].value + 'd100';

		pourcent =  Math.floor((Number(data.value) * event[3].value) + Number(event[1].value) - Number(event[2].value));
		
		let roll = new Roll(formula);
		roll.roll();
		// defining whether it's succes or failure and corresponding message
		if (event[0].value == 1) {
			if (Number(roll.result) >= 95) {
				message = "Echec critique !"; color = "firebrick";
			} else if (Number(roll.result) > Number(pourcent) && Number(roll.result) < 95) {
				message = "Echec !"; color = "darkred";
			} else if (Number(roll.result) <= 5) {
				message = "Réussite critique !"; color = "forestgreen";
			} else if (Number(roll.result) <= Number(pourcent) && Number(roll.result) >= 6) {
				message = "Réussite !"; color = "darkgreen";
			}
		} else {
			message = roll.result; color = "royalblue";
		}

		// creating dataset for the chat html template
		templateData = {
			title: data.label,
			skillValue: data.value,
			roll: roll.result,
			result: message,
			color: color,
			pourcent: Number(pourcent),
			bonus: event[1].value,
			malus: event[2].value,
			multi: event[3].value
		};
		//sending data to generate chat message
		this.rollMove(roll, actorData, data, templateData);

	} else if (rollmove == 'DIAL') {
		let template = 'systems/myrhias/templates/chat/roll-dialog.html';
		let dialogData = {
			title: data.label,
			skillValue: data.value,
			multiplicateur: data.multi,
		};

		const html = await renderTemplate(template, dialogData);
		return new Promise(resolve => {
			new Dialog({
				title: `Modificateur de jet`,
				content: html,
				buttons: {
					submit: {
						label: 'Faire rouler les dés !',
						callback: html => this._onRollable('MOVE', html[0].querySelectorAll('input'))
					}
				}
			}).render(true);
		})
	}
  }

  /**
   * Roll a move and use the chat card template.
   * @param {Object} templateData
   */
  rollMove(roll, actorData, dataset, templateData, form = null) {
    // Render the roll.
    let template = 'systems/myrhias/templates/chat/chat-move.html';
    renderTemplate(template, templateData).then(content => {
      if (roll) {
        roll.toMessage({ flavor: content });
      }
      else {
        ChatMessage.create({
		  isRollVisible: true,
          user: game.user._id,
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          content: content
        });
      }
    });
  }

  /**
  * Display money manager dialog
  * @param {Object} templateData
  */
 async _money(event) {
	const  actorData = this.actor.data;
	const  data = actorData.data;
	
	let template = 'systems/myrhias/templates/chat/pognon-dialog.html';
	let dialogData = {
		title: "Argent et banque",
		money: data.money
	};

	const html = await renderTemplate(template, dialogData);
	return new Promise(resolve => {
		new Dialog({
			title: `Gestionnaire de pognon`,
			content: html,
			buttons: {
				submit: {
					label: 'Enregistrer',
				}
			}
		}).render(true);
	})
  }

  distribute() {
	var 
		total = 25,
 		inputs = $('input[type="number"]');

	inputs
		.attr('max', total)
		.change(function() {
			console.log(inputs)
			//Make sure that current value is in range
			if($(this).val() > parseInt($(this).attr('max'))) {
				$(this).val($(this).attr('max'));
			} else if ($(this).val() < parseInt($(this).attr('min'))) {
				$(this).val($(this).attr('min'));
			}
			
			//Get currently available total
			var current = available();
			
			//Now update max on each input
			$('input').each(function(indx) {
				$(this).attr('max',  parseInt($(this).val()) + total - current);
			});
		});

		function available() {
		var sum = 0;
		inputs.each(function() {
			sum += parseInt($(this).val());
		});
		return sum;
		}
  }
  
}
